from datetime import timedelta

# Simple Jwt settings

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
    ],

}

SWAGGER_SETTINGS = {
    'USE_SESSION_AUTH': False,
    'SECURITY_DEFINITIONS': {
        'Bearer': {
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header'
        }
    }
}

SIMPLE_JWT = {
    "ACCESS_TOKEN_LIFETIME": timedelta(minutes=int(env.ACCESS_TOKEN_LIFETIME)),  # type: ignore
    "REFRESH_TOKEN_LIFETIME": timedelta(hours=float(env.REFRESH_TOKEN_LIFETIME)),  # type: ignore

    "ALGORITHM": "HS256",
    "SIGNING_KEY": env.SECRET_KEY,  # type: ignore

    "AUTH_HEADER_TYPES": ("Bearer",),
    "AUTH_HEADER_NAME": "HTTP_AUTHORIZATION",
    "USER_ID_FIELD": "id",
    "USER_ID_CLAIM": "user_id"
}

JWT_AUTH = {
    'JWT_AUTH_COOKIE': 'JWT',
}
