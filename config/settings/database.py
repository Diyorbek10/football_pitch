DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': env.DB_NAME,  # type: ignore
        'USER': env.DB_USER,  # type: ignore
        'HOST': env.DB_HOST,  # type: ignore
        'PORT': env.DB_PORT,  # type: ignore
        'PASSWORD': env.DB_PASS,  # type: ignore
    }
}
