import os
from dotenv import load_dotenv
import inspect

__all__ = ('env',)


class EnvConfig:
    DEBUG: bool = True
    PORT: int
    SECRET_KEY: str

    ACCESS_TOKEN_LIFETIME: int = 10  # minutes
    REFRESH_TOKEN_LIFETIME: float = 24  # hours

    DB_USER: str
    DB_PASS: str
    DB_NAME: str
    DB_PORT: int
    DB_HOST: str

    def __init__(self):
        load_dotenv()
        self.__get_variables()

    @classmethod
    def __get_variables(cls):
        attributes = inspect.getmembers(cls, lambda a: not (inspect.isroutine(a)))
        keys = dict(attributes).get('__annotations__').items()
        for k, t in keys:
            var = os.environ.get(k.upper()) or os.environ.get(k.lower(), dict(attributes).get(k))
            if t is not bool:
                setattr(cls, k, var)
            else:
                if var in ['False', 'false', 'FALSE']:
                    setattr(cls, k, False)
                else:
                    setattr(cls, k, bool(var))


env = EnvConfig()
