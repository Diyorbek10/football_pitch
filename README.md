
# Football Pitches

In this project, we can create a list of football pitches or order them for a certain time

There are 3 different types of users: \
Super Admin,\
Admin,\
User.\
To divide into roles, I used Django's default User model, if User is_super_user then it is Super Admin or if is_staff then Admin is otherwise simple User
## Installation

First, create an .env file similar to the .env.copy file

Create Virtual Environment
```bash
virtualenv venv
```
Install neccessary packages

```bash
  pip install -r requirements.txt
```

Install postgres with postgis exstension

```bash
  docker-compose up -d
```

Finally, install gdal for working with postgis

https://gdal.org/download.html

And add to settings.py following code:

```bash
  GDAL_LIBRARY_PATH = '/path/to/your/gdal/library'
```

## Demo

http://45.89.190.14/