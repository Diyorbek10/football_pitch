from rest_framework import serializers
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth.hashers import make_password

from main.models import Pitch, Image, Order


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', 'password', 'is_staff', 'is_superuser']
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        return user


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        exclude = ['created_at', 'updated_at']
        extra_kwargs = {
            'pitch': {'write_only': True}
        }


class PitchSerializer(serializers.ModelSerializer):
    images = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Pitch
        fields = ['id', 'name', 'address', 'contact', 'price', 'longitude', 'latitude', 'images', 'location']
        extra_kwargs = {
            'location': {'write_only': True}
        }

    def get_images(self, obj):
        return ImageSerializer(obj.images.all(), many=True, context=self.context).data


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        exclude = ['user', 'created_at']