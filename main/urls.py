from django.urls import path
from main.views import UserViewSet, PitchViewSet, ImageViewSet, OrderViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register('pitch', PitchViewSet, basename='pitch')
urlpatterns = [
                  path('user', UserViewSet.as_view()),
                  path('image', ImageViewSet.as_view()),
                  path('order', OrderViewSet.as_view())
              ] + router.urls
