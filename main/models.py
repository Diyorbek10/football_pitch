from django.db import models
from django.contrib.auth.models import AbstractUser, User
from django.contrib.gis.geos import Point
from django.contrib.gis.db import models as gis_models


# Create your models here.
class Pitch(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='pitches')
    name = models.CharField(max_length=150)
    address = models.CharField(max_length=250)
    contact = models.CharField(max_length=25)
    price = models.IntegerField()
    location = gis_models.PointField(geography=True, default=Point(0.0, 0.0))
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    @property
    def longitude(self):
        return self.location.x

    @property
    def latitude(self):
        return self.location.y


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='orders')
    pitch = models.ForeignKey(Pitch, on_delete=models.CASCADE, related_name='orders')
    started_date = models.DateTimeField()
    ended_date = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.get_full_name()


class Image(models.Model):
    pitch = models.ForeignKey(Pitch, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(upload_to="pitches/")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
