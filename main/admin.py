from django.contrib import admin

from main.models import Pitch, Order


# Register your models here.
@admin.register(Pitch)
class PitchAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'name', 'location', 'created_at']


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'pitch', 'started_date', 'ended_date']
