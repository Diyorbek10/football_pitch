from datetime import datetime

from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, filters
from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView, UpdateAPIView, DestroyAPIView
from rest_framework.response import Response
from django.contrib.auth.models import User
from rest_framework.permissions import AllowAny, IsAuthenticated

from main.models import Pitch, Image, Order
from main.permissions import IsAuthenticatedAndOwner
from main.serializers import UserSerializer, PitchSerializer, ImageSerializer, OrderSerializer
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from django.contrib.gis.geos import Point
from django.contrib.gis.db.models.functions import Distance
from rest_framework.parsers import MultiPartParser
from django.db.models import Q
from django.utils import timezone
from dateutil import parser

longitude = openapi.Parameter('longitude', openapi.IN_QUERY, type=openapi.TYPE_STRING)
latitude = openapi.Parameter('latitude', openapi.IN_QUERY, type=openapi.TYPE_STRING)
started_date = openapi.Parameter('started_date', openapi.IN_QUERY, type=openapi.FORMAT_DATETIME)
ended_date = openapi.Parameter('ended_date', openapi.IN_QUERY, type=openapi.FORMAT_DATETIME)


class UserViewSet(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [AllowAny]


class PitchViewSet(ListAPIView, UpdateAPIView, CreateAPIView, DestroyAPIView, RetrieveAPIView, viewsets.GenericViewSet):
    queryset = Pitch.objects.all()
    serializer_class = PitchSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]

    @swagger_auto_schema(manual_parameters=[longitude, latitude, started_date, ended_date])
    def list(self, request, *args, **kwargs):
        query = request.GET.get('ordering', None)
        _longitude = request.GET.get('longitude', None)
        _latitude = request.GET.get('latitude', None)
        _started_date = request.GET.get('started_date', None)
        _ended_date = request.GET.get('ended_date', None)
        if _started_date and _ended_date:
            _started_date = timezone.make_aware(parser.parse(_started_date))
            _ended_date = timezone.make_aware(parser.parse(_ended_date))

            self.queryset = self.queryset.exclude(
                Q(
                    Q(orders__started_date__range=(_started_date, _ended_date)) |
                    Q(orders__ended_date__range=(_started_date, _ended_date)) |
                    (Q(orders__started_date__lt=_ended_date) & Q(orders__ended_date__gt=_started_date))
                )
            )

        if query == 'location':
            if _latitude is None or _longitude is None:
                return Response({"error": "You need to enter longitude and latitude"}, status=400)

            user_location = Point(float(_longitude), float(_latitude), srid=4326)
            self.queryset = self.queryset.annotate(
                distance=Distance('location', user_location)
            ).order_by('-distance')
        return super().list(request, args, kwargs)

    def get_permissions(self):
        permission_classes = []
        if self.action == 'create' or self.action == 'update' or self.action == 'partial_update':
            permission_classes = [IsAuthenticatedAndOwner]
        elif self.action == 'list' or self.action == 'retrieve':
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'name': openapi.Schema(type=openapi.TYPE_STRING),
                'address': openapi.Schema(type=openapi.TYPE_STRING),
                'contact': openapi.Schema(type=openapi.TYPE_STRING),
                'price': openapi.Schema(type=openapi.TYPE_NUMBER),
                'longitude': openapi.Schema(type=openapi.TYPE_NUMBER),
                'latitude': openapi.Schema(type=openapi.TYPE_NUMBER),
            }
        )
    )
    def create(self, request, *args, **kwargs):
        _longitude = request.data.get('longitude', 0)
        _latitude = request.data.get('latitude', 0)
        location = Point(float(_longitude), float(_latitude))
        request.data['location'] = location
        return super().create(request, args, kwargs)


class ImageViewSet(CreateAPIView):
    parser_classes = (MultiPartParser,)
    queryset = Image.objects.all()
    serializer_class = ImageSerializer
    permission_classes = [IsAuthenticatedAndOwner]


class OrderViewSet(CreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
